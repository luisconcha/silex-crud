<?php
/**
 * File: bootstrap.php
 * Author: Luis Alberto Concha Curay
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 21/02/15
 * Time: 23:03
 * Project: silex
 * Copyright: 2015
 */

require_once 'vendor/autoload.php';
require_once 'doctrine.config.php';

$app = new \Silex\Application();

$app['debug']    = true;


$app->register( new \Silex\Provider\TwigServiceProvider(), array(
		'twig.path' => __DIR__ . '/src/LACC/views',
) );

$app->register( new \Silex\Provider\UrlGeneratorServiceProvider() );
$app->register( new \Silex\Provider\SessionServiceProvider() );

