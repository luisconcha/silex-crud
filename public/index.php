<?php
/**
 * File: index.php
 * Author: Luis Alberto Concha Curay
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 23/04/15
 * Time: 15:54
 * Project: silex_crud
 * Copyright: 2015
 */

require_once __DIR__ . '/../bootstrap.php';
require_once __DIR__ . '/../src/LACC/routers/cliente_router.php';

//rescrita de metodos http
\Symfony\Component\HttpFoundation\Request::enableHttpMethodParameterOverride();

$app->get( '/', function() use( $app ){

		return $app['twig']->render( 'index.twig', [] );
} )
		->bind( 'index' );


$app->run();