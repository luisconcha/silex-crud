<?php
/**
 * File: ClienteService.php
 * Author: Luis Alberto Concha Curay
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 23/04/15
 * Time: 18:09
 * Project: silex_crud
 * Copyright: 2015
 */

namespace LACC\App\Service;

use Doctrine\ORM\EntityManager;
use LACC\App\Interfaces\ICliente;
use LACC\App\Entity\Cliente as ClienteEntity;


class ClienteService implements ICliente
{
		private $em;

		public function __construct( EntityManager $em )
		{
				$this->em = $em;
		}

		function insert(array $data)
		{
				$clienteEntity = new ClienteEntity();
				$clienteEntity->setNome( $data['nome'] );
				$clienteEntity->setEmail( $data['email'] );

				$this->em->persist( $clienteEntity );
				$this->em->flush();

				return $clienteEntity;
		}

		function update( array $data)
		{
				$cliente = $this->em->getReference( 'LACC\App\Entity\Cliente', $data['id'] );
				$cliente->setNome( $data['nome'] );
				$cliente->setEmail( $data['email'] );

				$this->em->persist( $cliente );
				$this->em->flush();

				return $cliente;
		}

		function delete($id)
		{
				$cliente = $this->em->getReference( 'LACC\App\Entity\Cliente', $id );
				$this->em->remove( $cliente );
				$this->em->flush();

				return $cliente;
		}

		public function fecthAll()
		{
				$data = $this->getRepo()->getListClientesOrderName();
				return $data;
		}

		public function findId( $id )
		{
				$data = $this->getRepo()->find( $id );
				return $data;
		}

		public function getRepo()
		{
				return $this->em->getRepository( 'LACC\App\Entity\Cliente' );
		}
}