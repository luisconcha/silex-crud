Silex - Crud
====================
    - Project crud sample using SILEX  and Doctrine

1) Installing
--------------

	- git clone https://luisconcha@bitbucket.org/luisconcha/silex-crud.git  silex_crud
	- php composer.phar self-udpate
	- php composer.phar install

2) Folder Structure and initial files:
-------------------------------------

	* silex
      - public/
      - ------- /assets
      - ------- index.php
      - ------- .htaccess
      - sql/	
      - src/
      - ----LACC/
      - ---------App/
      - ---------Interfaces/
      - ---------Repository/
      - ---------Entity/
      - ---------Service/
      - ----routers/
      - -----------/cliente_router.php
      - views/
      - -----clientes/
      - -------------list.twig
      - -------------new.twig
      - -------------update.twig
      - ------index.twig
      - ------layout.twig
      - vendor/
      - bootstrap.php
      - cli-config.php
      - composer.json
      - composer.phar
      - doctrine.config.php
      - README.md
	
	
3) Creating virtual host
------------------------
```sh
	<VirtualHost *:80>
		DocumentRoot "/var/www/html/silex_crud/public"
		ServerName silex.dev.com
		<Directory "/var/www/html/silex_crud/public">
			AllowOverride All
		</Directory>
	</VirtualHost>
```

4) Creating .htaccess
----------------------
```sh
	<IfModule mod_rewrite.c>
		Options -MultiViews
		RewriteEngine On
		RewriteCond %{REQUEST_FILENAME} !-f
		RewriteRule ^ index.php [L]
	</IfModule>
```
5) Server configuration (embedded web server)
--------------------------------------------
    - cd/path_project/public
    - php -S 127.0.0.1:9090

6) Creating BD
----------------------
    - Run the dump database: silex_crud/dump.sq

### Version
1.0.    

### Developer:
Luis Alberto Concha Curay - luisconchacuray@gmail.com	