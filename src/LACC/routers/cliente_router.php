<?php
/**
 * File: cliente_router.php
 * Author: Luis Alberto Concha Curay
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 23/04/15
 * Time: 15:56
 * Project: silex_crud
 * Copyright: 2015
 */

use LACC\App\Service\ClienteService;
use Symfony\Component\HttpFoundation\Request;

$app['clienteService'] = function() use($em){

		$clienteService = new ClienteService($em);
		return $clienteService;
};

$app->get('/clientes', function() use($app){

		$dados = $app['clienteService']->fecthAll();

		return $app['twig']->render('clientes/list.twig', array('clientes'=>$dados));
})
		->bind('clientes');

$app->get( 'clienteNew', function() use( $app ){
		return $app['twig']->render( 'clientes/new.twig' );
})
		->bind( 'clienteNew' );

$app->post( 'addCliente', function( Request $request ) use($app){

		if( 'POST' == $request->getMethod() ) {

				$nome  = $request->get( 'nome' );
				$email = $request->get( 'email' );

				$data  = [
						'nome' => $nome,
						'email'=> $email
				];
				$insert = $app['clienteService']->insert( $data );

				if( $insert ){
						$app['session']->getFlashBag()->add('success', 'Registro inserido com sucesso!!!');
				}else{
						$app['session']->getFlashBag()->add('error', 'Houve um problema ao tentar inserir o registro: '.$data['nome']);
				}
				return $app->redirect('clientes');
		}
});


$app->get( '/clienteUpdate/{id}', function( $id ) use( $app ){

		$dataCliente = $app['clienteService']->findId( $id );

		return $app['twig']->render('clientes/update.twig', array( 'cliente'=>$dataCliente ));
})
		->bind('clienteUpdate');

$app->put('updateCliente', function( Request $request ) use( $app ){

		if( 'PUT' == $request->getMethod() ) {
		 	  $nome  = $request->get( 'nome' );
		 	  $email = $request->get( 'email' );
				$id    = $request->get( 'id' );
				$data  = [
						'nome' => $nome,
						'email'=> $email,
						'id'   => $id
				];
				$update = $app['clienteService']->update( $data );

				if( $update ){
						$app['session']->getFlashBag()->add('success', 'Registro alterado com sucesso!!!');
				}else{
						$app['session']->getFlashBag()->add('error', 'Houve um problema ao tentar altarar o registro: '.$data['nome']);
				}
				return $app->redirect('clientes');
		}

})
		->bind('updateCliente');

$app->delete('/clienteDelete/{id}', function(Request $request) use( $app ){
		if( 'DELETE' == $request->getMethod() ) {
				$id     = (int)$request->get( 'id' );
				$delete = $app['clienteService']->delete( $id );

				if( $delete )
						return json_encode('deletou');
				else
						return json_encode('ndeletou');
		}
})
		->method('DELETE')
		->bind('clienteDelete');