<?php
/**
 * File: ICliente.php
 * Author: Luis Alberto Concha Curay
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 22/02/15
 * Time: 01:56
 * Project: silex
 * Copyright: 2015
 */

namespace LACC\App\Interfaces;


interface ICliente
{

		function insert( array $data );

		function update( array $array );

		function delete( $id );
}