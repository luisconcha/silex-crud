<?php
/**
 * File: ClienteRepository.php
 * Author: Luis Alberto Concha Curay
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 23/04/15
 * Time: 18:08
 * Project: silex_crud
 * Copyright: 2015
 */

namespace LACC\App\Repository;


use Doctrine\ORM\EntityRepository;

class ClienteRepository extends EntityRepository
{
		public function getListClientesOrderName()
		{
				return $this->createQueryBuilder( 'c' )->orderBy( 'c.nome', 'asc' )
																							 ->getQuery()
																							 ->getResult();

		}


}