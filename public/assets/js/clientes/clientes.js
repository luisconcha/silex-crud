$(document).ready(function(){
    var direccion   = window.location;
    var base_url    = direccion.protocol + "//" + direccion.host + "/";
    var tblClientes = $('#tblClientes');

    tblClientes.on( 'click', '#btnDelete', function (e) {
        e.preventDefault();

        var idCliente = $(this).attr('data-id');

        bootbox.confirm("Deseja deletar o registro?", function(resul){
            if( resul == true ){
                $.ajax({
                    url: base_url+'clienteDelete/'+idCliente,
                    type: 'delete',
                    success: function(data){

                        if( data == '"deletou"' ){
                            bootbox.dialog({
                                message: "Registro deletado com sucesso!",
                                title: "Silex sample",
                                buttons:{
                                    success: {
                                        label: "Fechar janela",
                                        className: "btn btn-success",
                                        callback: function(){
                                            $(location).attr('href','/clientes');
                                        }
                                    }
                                }
                            })
                        }else if( data == '"ndeletou"'){
                            bootbox.dialog({
                                message: "Erro ao tentar deletar o registro!",
                                title: "Silex sample",
                                buttons:{
                                    success: {
                                        label: "Fechar janela",
                                        className: "btn btn-danger",
                                        callback: function(){
                                            $(location).attr('href','/clientes');
                                        }
                                    }
                                }
                            })
                        }
                    }
                })
            }
        });
    } )

});