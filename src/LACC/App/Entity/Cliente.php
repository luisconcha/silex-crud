<?php
/**
 * File: Cliente.php
 * Author: Luis Alberto Concha Curay
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 23/04/15
 * Time: 18:04
 * Project: silex_crud
 * Copyright: 2015
 */

namespace LACC\App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="LACC\App\Repository\ClienteRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="tb_cliente")
 */
class Cliente
{
		/**
		 * @ORM\Id()
		 * @ORM\Column(type="integer")
		 * @ORM\GeneratedValue()
		 */
		private $id;

		/**
		 * @ORM\Column(type="string", length=255)
		 */
		private $nome;

		/**
		 * @ORM\Column(type="string", length=255)
		 */
		private $email;

		/**
		 * @var \DateTime
		 *
		 * @ORM\Column(name="created_at", type="datetime", nullable=true)
		 */
		private $createdAt;

		/**
		 * @var \DateTime
		 *
		 * @ORM\Column(name="update_at", type="datetime", nullable=true)
		 */
		private $updateAt;


		public function __construct()
		{
				$this->createAt = new \DateTime('now');
				$this->updateAt = new \DateTime('now');
		}

		/**
		 * @return mixed
		 */
		public function getId()
		{
				return $this->id;
		}


		/**
		 * @return mixed
		 */
		public function getNome()
		{
				return $this->nome;
		}

		/**
		 * @param mixed $nome
		 */
		public function setNome($nome)
		{
				$this->nome = $nome;
		}

		/**
		 * @return mixed
		 */
		public function getEmail()
		{
				return $this->email;
		}

		/**
		 * @param mixed $email
		 */
		public function setEmail($email)
		{
				$this->email = $email;
		}

		public function getUpdatedAt() {
				return $this->updateAt;
		}

		/**
		 * @ORM\PreUpdate()
		 */
		public function setUpdatedAt() {
				$this->updatedAt = new \DateTime("now");
		}

		public function getCreatedAt() {
				return $this->createdAt;
		}

		/**
		 * @ORM\PrePersist()
		 */
		public function setCreatedAt() {
				$this->createdAt = new \DateTime("now");
		}
}